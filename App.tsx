import * as React from 'react';
import Home from './screens/Home';
import {SafeAreaView} from 'react-native';
import {BLEProvider} from './contexts/BLEcontext';
import tw from 'tailwind-react-native-classnames';

const App = () => {
  return (
    <BLEProvider>
      <SafeAreaView style={[tw`flex-1 bg-gray-100`]}>
        <Home />
      </SafeAreaView>
    </BLEProvider>
  );
};

export default App;

// (
//     <>
//       {listData.length
//         ? listData.map((d, i) => {
//             return (
//               <View key={i}>
//                 <Text style={styles.title}>
//                   Thời gian đo: {d.date.value}/{d.month.value}/
//                   {d.year.value} {d.hour.value}:{d.minute.value}:
//                   {d.second.value}
//                 </Text>
//                 <Text>
//                   {d.deviceName.title}: {d.deviceName.default}
//                 </Text>
//                 <Text>
//                   {d.systolic.title}: {d.systolic.value}
//                 </Text>
//                 <Text>
//                   {d.diastole.title}: {d.diastole.value}
//                 </Text>
//                 <Text>
//                   {d.heartbeat.title}: {d.heartbeat.value}
//                 </Text>
//                 <Text>------------------------------------</Text>
//                 <Text> </Text>
//               </View>
//             );
//           })
//         : ''}
//       {/* <PulseIndicator />
//       <Text style={styles.heartRateTitleText}>Your Heart Rate Is:</Text>
//       <Text style={styles.heartRateText}>{heartRate} bpm</Text> */}
//     </>
//   )
