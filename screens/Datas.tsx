import * as React from 'react';
import {ScrollView, Text, View} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import {useBLEContext} from '../contexts/BLEcontext';
import moment from 'moment';

export interface IDatasProps {}

export default function Datas() {
  const {listData} = useBLEContext();

  return (
    <>
      <Text style={[tw`text-center text-2xl font-bold`]}>List Data</Text>
      <ScrollView style={[tw`p-4`]}>
        {listData.map(d => {
          return (
            <View key={d.baseValue} style={[tw`border-b pb-2 mb-4`]}>
              <Text style={[tw``]}>Tên thiết bị: {d.deviceName}</Text>
              <Text style={[tw``]}>Tâm trương: {d.diastole}</Text>
              <Text style={[tw``]}>Tâm thu: {d.systolic}</Text>
              <Text style={[tw``]}>Nhịp tim: {d.heartbeat}</Text>
              <Text style={[tw``]}>
                Thời gian: {moment(d.logTime).format('DD/MM/YYYY hh:mm:ss')}
              </Text>
              <Text style={[tw``]}>Ngưới dùng: {d.user}</Text>
            </View>
          );
        })}
      </ScrollView>
    </>
  );
}
