// import {useTailwind} from 'nativewind';
import * as React from 'react';
import {Text, TouchableHighlight, View} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import Devices from './Devices';
import Datas from './Datas';

export interface IHomeProps {}

export default function Home() {
  const [page, setPage] = React.useState<string>('device');

  return (
    <>
      {page === 'device' ? <Devices /> : <Datas />}
      <View style={[tw`flex flex-row `]}>
        <TouchableHighlight
          onPress={() => setPage('device')}
          style={[tw`w-1/2 text-center`]}>
          <Text style={[tw`text-center`]}>Device</Text>
        </TouchableHighlight>
        <TouchableHighlight
          onPress={() => setPage('data')}
          style={[tw`w-1/2 text-center`]}>
          <Text style={[tw`text-center`]}>Data</Text>
        </TouchableHighlight>
      </View>
    </>
  );
}
