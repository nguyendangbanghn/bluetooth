import * as React from 'react';
import {ScrollView, Text, TouchableHighlight} from 'react-native';
import tw from 'tailwind-react-native-classnames';
import {useBLEContext} from '../contexts/BLEcontext';
import {Device} from 'react-native-ble-plx';

export interface IDevicesProps {}

export default function Devices() {
  const {allDevices, deviceId, chooseDevice, scanForPeripherals} =
    useBLEContext();
  React.useEffect(() => {
    scanForPeripherals();
  }, []);
  return (
    <>
      <Text style={[tw`text-center text-2xl font-bold`]}>List Device</Text>
      <ScrollView style={[tw`p-4`]}>
        {allDevices.map((device: Device) => {
          return (
            <TouchableHighlight
              style={[
                tw`rounded-xl border border-gray-300 mb-2 p-2 ${
                  device.id === deviceId ? 'bg-blue-200' : ''
                }`,
              ]}
              key={device.id}
              onPress={() => chooseDevice(device.id)}>
              <Text style={[tw`rounded-full ml-2 justify-between text-xl`]}>
                {device.name}
              </Text>
            </TouchableHighlight>
          );
        })}
      </ScrollView>
    </>
  );
}
