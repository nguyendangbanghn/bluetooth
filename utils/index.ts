import {atob} from 'react-native-quick-base64';

const utils = {
  base64ToHex(str: string) {
    const raw = atob(str);
    let result = [];
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result.push(String(hex.length === 2 ? hex : '0' + hex).toUpperCase());
    }
    return result;
  },
};

export default utils;
