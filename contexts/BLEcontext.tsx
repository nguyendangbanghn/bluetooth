import React, {useCallback, useEffect, useState} from 'react';
import {AsyncStorage, PermissionsAndroid, Platform} from 'react-native';
import {
  BleManager,
  Characteristic,
  Device,
  Service,
} from 'react-native-ble-plx';
import DeviceInfo from 'react-native-device-info';
import {PERMISSIONS, requestMultiple} from 'react-native-permissions';
import {beurerBM85Services} from '../devices/beurerBm85';

type TData = {
  deviceId?: string;
  baseValue: string;
  deviceName: string;
  systolic: number;
  diastole: number;
  heartbeat: number;
  user: number;
  logTime: string;
};

type BLEContextType = {
  allDevices: Device[];
  listData: TData[];
  deviceId: string;
  chooseDevice: (device: string) => {};
  scanForPeripherals: () => {};
  stopScan: () => {};
};
const errorHandler = (err: any, prefix?: string) => {
  if (err) {
    console.error(`____${prefix || 'ERROR'}____`, err);
  }
};

type VoidCallback = (result: boolean) => void;

const bleManager = new BleManager();

const BLEContext = React.createContext<BLEContextType | any>({});

const requestPermissions = async (cb: VoidCallback) => {
  if (Platform.OS === 'android') {
    const apiLevel = await DeviceInfo.getApiLevel();

    if (apiLevel < 31) {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: 'Bluetooth Low Energy requires Location',
          buttonNeutral: 'Ask Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      cb(granted === PermissionsAndroid.RESULTS.GRANTED);
    } else {
      const result = await requestMultiple([
        PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
        PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      ]);

      const isGranted =
        result['android.permission.BLUETOOTH_CONNECT'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.BLUETOOTH_SCAN'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        result['android.permission.ACCESS_FINE_LOCATION'] ===
          PermissionsAndroid.RESULTS.GRANTED;

      cb(isGranted);
    }
  } else {
    cb(true);
  }
};
const isDuplicteDevice = (devices: Device[], nextDevice: Device) =>
  devices.findIndex(device => nextDevice.id === device.id) > -1;

export function BLEProvider(props: {children: any}) {
  const [allDevices, setAllDevices] = useState<Device[]>([]);
  const [deviceId, setDeviceId] = useState<string>('');
  const [device, setDevice] = useState<Device | null>(null);
  const [listData, setListData] = useState<any[]>([]);

  useEffect(() => {
    AsyncStorage.getItem('deviceId').then(value => setDeviceId(value || ''));
  }, []);

  //   useEffect(() => {
  //     const deviceWillConnect = allDevices.find(d => d.id === deviceId);
  //     console.log('DEVICE_CONNECT', deviceWillConnect?.name);
  //     console.log(deviceWillConnect?.isConnectable);
  //     if (
  //       deviceWillConnect &&
  //       deviceWillConnect.id !== device?.id &&
  //       deviceWillConnect.isConnectable
  //     ) {
  //       bleManager
  //         .connectToDevice(deviceId)
  //         .then((deviceConnect: Device) => {
  //           stopScan();
  //           deviceConnect
  //             .discoverAllServicesAndCharacteristics()
  //             .then(() => {
  //               setDevice(deviceConnect);
  //               connectHandler(deviceConnect);
  //             })
  //             .catch((reason: any) => {
  //               errorHandler(reason, 'DISCOVERY');
  //             });
  //           deviceConnect.onDisconnected(err => {
  //             errorHandler(err, 'DISCONNECT');
  //             scanForPeripherals();
  //             console.log('BLUETOOTH_DISCONNECT_SUCCESS');
  //             setDevice(null);
  //           });
  //         })
  //         .catch(err => {
  //           errorHandler(err.message, 'CONNECT_ERROR');
  //           stopScan();
  //           scanForPeripherals();
  //         });
  //     }
  //   }, [allDevices, deviceId, device]);

  useEffect(() => {
    if (deviceId) {
      bleManager
        .connectToDevice(deviceId)
        .then(deviceConnect => {
          deviceConnect
            .discoverAllServicesAndCharacteristics()
            .then(() => {
              console.log(
                '________________device____name_______',
                deviceConnect.name,
              );
              setDevice(deviceConnect);
              connectHandler(deviceConnect);
            })
            .catch((reason: any) => {
              errorHandler(reason, 'DISCOVERY');
            });
        })
        .catch(err => {
          console.log('CONNECT_ERROR', err);
        });
    }
  }, [deviceId]);

  const connectHandler = (deviceConnect: Device) => {
    console.log('CONNECT_SUCCESS');
    switch (deviceConnect.name) {
      case 'Beurer BM85':
        deviceConnect.monitorCharacteristicForService(
          '00001810-0000-1000-8000-00805f9b34fb',
          '00002a35-0000-1000-8000-00805f9b34fb',
          (err, characteristic: Characteristic | null) => {
            errorHandler(err);
            if (characteristic) {
              const value = beurerBM85Services.readData(
                String(characteristic.value),
              );
              value.deviceId = deviceConnect.id;

              setListData(state => {
                const obj = state.reduce((data, element) => {
                  data[element.baseValue] = element;
                  return data;
                }, {});

                obj[value.baseValue] = value;

                return Object.values(obj);
              });
            }
          },
        );
        break;
      case 'HEM-7143T1':
        deviceConnect.services().then(async (services: Service[]) => {
          const value1 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a29-0000-1000-8000-00805f9b34fb',
          );

          const value2 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a24-0000-1000-8000-00805f9b34fb',
          );

          const value3 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a25-0000-1000-8000-00805f9b34fb',
          );

          const value4 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a26-0000-1000-8000-00805f9b34fb',
          );

          const value5 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a28-0000-1000-8000-00805f9b34fb',
          );

          const value6 = await deviceConnect.readCharacteristicForService(
            '0000180a-0000-1000-8000-00805f9b34fb',
            '00002a23-0000-1000-8000-00805f9b34fb',
          );

          //   const value7 = await deviceConnect.readCharacteristicForService(
          //     '0000fe4a-0000-1000-8000-00805f9b34fb',
          //     'db5b55e0-aee7-11e1-965e-0002a5d5c51b',
          //   );
          //   console.log('____________read__7', value7);

          deviceConnect.monitorCharacteristicForService(
            '0000fe4a-0000-1000-8000-00805f9b34fb',
            'b305b680-aee7-11e1-a730-0002a5d5c51b',
            (err, characteristic: Characteristic) => {
              if (err) {
                console.log('mornitor__err___1', err);
              }
              console.log('Mornitor____________1', characteristic);
            },
          );

          deviceConnect.monitorCharacteristicForService(
            '0000fe4a-0000-1000-8000-00805f9b34fb',
            '49123040-aee8-11e1-a74d-0002a5d5c51b',
            (err, characteristic: Characteristic) => {
              if (err) {
                console.log('mornitor__err___2', err);
              }
              console.log('Mornitor____________2', characteristic);
            },
          );
          //   deviceConnect.monitorCharacteristicForService(
          //     '0000fe4a-0000-1000-8000-00805f9b34fb',
          //     '49123040-aee8-11e1-a74d-0002a5d5c51b',
          //     (err, characteristic: Characteristic) => {
          //       console.log('Mornitor____________2', characteristic);
          //     },
          //   );
          //   const cList: any[] = [];
          //   for (const service of services) {
          //     const characteristics = await service.characteristics();
          //     cList.push(JSON.parse(JSON.stringify(characteristics)));
          //   }
          //   console.log(
          //     '_____________123123123123123_________________\n\n',
          //     cList,
          //   );
          //   services.map(async (s: Service) => {
          //     const characteristics = await s.characteristics();
          //     return JSON.stringify(characteristics);
          //   });
        });
        break;
      default:
        break;
    }
  };

  const scanForPeripherals = () => {
    requestPermissions(isGranted => {
      if (isGranted) {
        console.log('START_SCAN_DEVICE...');
        bleManager.startDeviceScan(null, null, (error, deviceScan) => {
          errorHandler(error);
          if (deviceScan && deviceScan.name) {
            setAllDevices((prevState: Device[]) => {
              if (!isDuplicteDevice(prevState, deviceScan)) {
                return [...prevState, deviceScan];
              }
              return prevState;
            });
          }
        });
      }
    });
  };

  const stopScan = () => {
    bleManager.stopDeviceScan();
  };

  const chooseDevice = useCallback((_deviceId: string) => {
    setDeviceId(_deviceId);
    AsyncStorage.setItem('deviceId', _deviceId);
  }, []);

  const values = React.useMemo(
    () => ({
      scanForPeripherals,
      chooseDevice,
      stopScan,
      allDevices,
      listData,
      deviceId,
    }),
    [allDevices, listData, deviceId, chooseDevice],
  );

  return (
    <BLEContext.Provider value={values}>{props.children}</BLEContext.Provider>
  );
}

export const useBLEContext = () => {
  const context = React.useContext<BLEContextType>(BLEContext);
  if (!context) {
    throw new Error('useBLE must be used within a BLEProvider');
  }
  return context;
};
