import moment from 'moment';
import utils from '../utils';

const mapData = {
  deviceName: {
    byteIndex: [0],
    title: 'Tên thiết bị',
    default: 'Beurer BM85',
  },
  systolic: {
    byteIndex: [2, 1],
    title: 'Tâm thu',
  },
  diastole: {
    byteIndex: [4, 3],
    title: 'Tâm trương',
  },
  meanArterialPressure: {
    byteIndex: [6, 5],
    title: 'Áp lực động mạch trung bình',
    note: 'Không hỗ trợ trên BM85',
  },
  year: {
    byteIndex: [8, 7],
    title: 'Năm',
  },
  month: {
    byteIndex: [9],
    title: 'Tháng',
  },
  date: {
    byteIndex: [10],
    title: 'Ngày',
  },
  hour: {
    byteIndex: [11],
    title: 'Giờ',
  },
  minute: {
    byteIndex: [12],
    title: 'Phút',
  },
  second: {
    byteIndex: [13],
    title: 'Giây',
  },
  heartbeat: {
    byteIndex: [14, 15],
    title: 'Nhịp tim',
  },
  user: {
    byteIndex: [16],
    title: 'Người dùng',
  },
  measurementState: {
    byteIndex: [17],
    title: 'Trạng thái đo lường',
  },
};

export const beurerBM85Services = {
  readData: (value: string) => {
    const hexValue = utils.base64ToHex(value);
    const objData: any = {...mapData};

    Object.keys(objData).forEach(k => {
      objData[k].hexValue = objData[k].byteIndex
        .map((i: number) => hexValue[i])
        .join('');
      objData[k].value = parseInt(objData[k].hexValue, 16);
    });

    const result: {
      deviceId?: string;
      baseValue: string;
      deviceName: string;
      systolic: number;
      diastole: number;
      heartbeat: number;
      user: number;
      logTime: string;
    } = {
      baseValue: value,
      deviceName: objData.deviceName.default,
      systolic: objData.systolic.value,
      diastole: objData.diastole.value,
      user: objData.user.value + 1,
      heartbeat: objData.heartbeat.value,
      logTime: moment(
        `${objData.year.value}/${objData.month.value}/${objData.date.value} ${objData.hour.value}:${objData.minute.value}:${objData.second.value}`,
        'YYYY/M/D h:m:s',
      ).toISOString(),
    };

    return result;
  },
};
