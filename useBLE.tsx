/* eslint-disable no-bitwise */
import {useState} from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import {
  BleError,
  BleManager,
  Characteristic,
  Device,
} from 'react-native-ble-plx';
import {PERMISSIONS, requestMultiple} from 'react-native-permissions';
import DeviceInfo from 'react-native-device-info';

import {atob} from 'react-native-quick-base64';

// const HEART_RATE_UUID = '0000180d-0000-1000-8000-00805f9b34fb';
// const HEART_RATE_CHARACTERISTIC = '00002a37-0000-1000-8000-00805f9b34fb';

const bleManager = new BleManager();

type VoidCallback = (result: boolean) => void;

interface BluetoothLowEnergyApi {
  scanForPeripherals(): void;
  connectToDevice: (deviceId: Device) => Promise<void>;
  disconnectFromDevice: () => void;
  connectedDevice: Device | null;
  allDevices: Device[];
  heartRate: number;
  listData: any[];
}

function useBLE(): BluetoothLowEnergyApi {
  const [allDevices, setAllDevices] = useState<Device[]>([]);
  const [connectedDevice, setConnectedDevice] = useState<Device | null>(null);
  const [heartRate, setHeartRate] = useState<number>(0);
  const [listData, setListData] = useState<any[]>([]);

  const requestPermissions = async (cb: VoidCallback) => {
    if (Platform.OS === 'android') {
      const apiLevel = await DeviceInfo.getApiLevel();

      if (apiLevel < 31) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message: 'Bluetooth Low Energy requires Location',
            buttonNeutral: 'Ask Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        cb(granted === PermissionsAndroid.RESULTS.GRANTED);
      } else {
        const result = await requestMultiple([
          PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
          PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
          PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ]);

        const isGranted =
          result['android.permission.BLUETOOTH_CONNECT'] ===
            PermissionsAndroid.RESULTS.GRANTED &&
          result['android.permission.BLUETOOTH_SCAN'] ===
            PermissionsAndroid.RESULTS.GRANTED &&
          result['android.permission.ACCESS_FINE_LOCATION'] ===
            PermissionsAndroid.RESULTS.GRANTED;

        cb(isGranted);
      }
    } else {
      cb(true);
    }
  };

  const isDuplicteDevice = (devices: Device[], nextDevice: Device) =>
    devices.findIndex(device => nextDevice.id === device.id) > -1;

  const scanForPeripherals = () => {
    requestPermissions(isGranted => {
      if (isGranted) {
        bleManager.startDeviceScan(null, null, (error, device) => {
          if (error) {
            console.log('ERROR_SCAN_DEVICE', error);
          }

          if (device && device.name) {
            setAllDevices((prevState: Device[]) => {
              if (!isDuplicteDevice(prevState, device)) {
                return [...prevState, device];
              }
              return prevState;
            });
          }
        });
      }
    });
  };

  const connectToDevice = async (device: Device) => {
    try {
      const deviceConnection = await bleManager.connectToDevice(device.id, {
        autoConnect: true,
      });
      setConnectedDevice(deviceConnection);
      await deviceConnection.discoverAllServicesAndCharacteristics();
      bleManager.stopDeviceScan();
      startStreamingData(deviceConnection);
    } catch (e) {
      console.log('FAILED TO CONNECT', e);
    }
  };

  const disconnectFromDevice = () => {
    if (connectedDevice) {
      bleManager.cancelDeviceConnection(connectedDevice.id);
      setConnectedDevice(null);
      setHeartRate(0);
    }
  };

  const startStreamingData = async (device: Device) => {
    if (device) {
      function base64ToHex(str: string) {
        const raw = atob(str);
        let result = [];
        for (let i = 0; i < raw.length; i++) {
          const hex = raw.charCodeAt(i).toString(16);
          result.push(String(hex.length === 2 ? hex : '0' + hex).toUpperCase());
        }
        return result;
      }

      device.monitorCharacteristicForService(
        '00001810-0000-1000-8000-00805f9b34fb',
        '00002a35-0000-1000-8000-00805f9b34fb',
        (_err, characteristic) => {
          if (characteristic?.value) {
            const data: any = {
              deviceName: {
                byteIndex: [0],
                title: 'Tên thiết bị',
                value: device.name,
                default: device.name,
              },
              systolic: {
                byteIndex: [2, 1],
                title: 'Tâm thu',
              },
              diastole: {
                byteIndex: [4, 3],
                title: 'Tâm trương',
              },
              meanArterialPressure: {
                byteIndex: [6, 5],
                title: 'Áp lực động mạch trung bình',
                note: 'Không hỗ trợ trên BM85',
              },
              year: {
                byteIndex: [8, 7],
                title: 'Năm',
              },
              month: {
                byteIndex: [9],
                title: 'Tháng',
              },
              date: {
                byteIndex: [10],
                title: 'Ngày',
              },
              hour: {
                byteIndex: [11],
                title: 'Giờ',
              },
              minute: {
                byteIndex: [12],
                title: 'Phút',
              },
              second: {
                byteIndex: [13],
                title: 'Giây',
              },
              heartbeat: {
                byteIndex: [14, 15],
                title: 'Nhịp tim',
              },
              user: {
                byteIndex: [16],
                title: 'Người dùng',
              },
              measurementState: {
                byteIndex: [17],
                title: 'Trạng thái đo lường',
              },
            };
            const value = base64ToHex(String(characteristic?.value));
            Object.keys(data).forEach(k => {
              data[k].hexValue = data[k].byteIndex
                .map((i: number) => value[i])
                .join('');
              data[k].value = parseInt(data[k].hexValue, 16);
            });
            console.log('_____________data', data);
            setListData(state => [...state, data]);
          }
        },
      );
    } else {
      console.log('No Device Connected');
    }
  };

  return {
    scanForPeripherals,
    connectToDevice,
    allDevices,
    connectedDevice,
    disconnectFromDevice,
    heartRate,
    listData,
  };
}

export default useBLE;
