module.exports = {
  content: ['./App.{js,jsx,ts,tsx}', './src/screens/**.{js,jsx}'],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: require('tailwind-rn/unsupported-core-plugins'),
};
